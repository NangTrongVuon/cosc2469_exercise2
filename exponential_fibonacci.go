package main


func main() {
	println("Test functions below")

	testValues := [5] int {5, 10, 15, 20, 25}

	for i := 0; i < 5; i++ {
	//	recursiveExponentiation(10, testValues[i])
	//	iterativeExponentiation(10, testValues[i])
		recursiveLogarithmExponentiation(10, testValues[i])
	//	iterativeLogarithmExponentiation(10, testValues[i])
		println(recursiveLogarithmExponentiation(2, testValues[i]))
	//	fibonacciIterative(testValues[i])
	}

}


func recursiveExponentiation(x float64, n int) float64 {

	// base case: n = 1
	if n == 0.0 { return 1.0 }
	if n == 1.0 {
		return x
	} else {
		return x * recursiveExponentiation(x, n-1)
	}

}

func iterativeExponentiation(x float64, n int) float64 {

	if n == 0 { return 1 }

	result := 1.0

	for n >= 1 {
		result *= x
		n -= 1
	}

	return result

}

func recursiveLogarithmExponentiation(x float64, n int) float64 {

	if n == 0 {
		return 1
	}
	if n == 1 {
		return x
	} else {
		result := recursiveLogarithmExponentiation(x, n/2)

		if n % 2 == 0 {
			return result * result
		} else {
			return result * result * x
		}
	}

}

func iterativeLogarithmExponentiation(x float64, n int) float64 {


	if n == 0 { return 1 }

	result := 1.0

	for n > 1 {
		if n % 2 == 0 {
			x *= x
			n /= 2
		} else {
			result = x * result
			x *= x
			n = (n - 1) / 2
		}
	}

	return x * result
}

func fibonacciRecursive(n int) int {


	if n == 0 { return 0 }
	if n == 1 {
		return 1
	} else {
		return fibonacciRecursive(n - 1) + fibonacciRecursive(n - 2)
	}
}

func fibonacciIterative(n int) int {

	f1 := 0
	f2 := 1

	for i := 0; i < n; i++ {
		f1, f2 = f2, f1 + f2
	}

	return f1

}


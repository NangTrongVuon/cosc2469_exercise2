package main

import "testing"

func benchmarkRecursiveExponential(n int, b *testing.B) {
	for i := 0; i < b.N; i++ {
		recursiveExponentiation(2, n)
	}
}

func benchmarkRecursiveLogarithmExponential(n int, b *testing.B) {
	for i := 0; i < b.N; i++ {
		recursiveLogarithmExponentiation(2, n)
	}
}

func benchmarkIterativeExponential(n int, b *testing.B) {
	for i := 0; i < b.N; i++ {
		iterativeExponentiation(2, n)
	}
}

func benchmarkIterativeLogarithmExponential(n int, b *testing.B) {
	for i := 0; i < b.N; i++ {
		iterativeLogarithmExponentiation(2, n)
	}
}

func benchmarkRecursiveFibonacci(n int, b *testing.B) {
	for i := 0; i < b.N; i++ {
		fibonacciRecursive(n)
	}
}

func benchmarkIterativeFibonacci(n int, b *testing.B) {
	for i := 0; i < b.N; i++ {
		fibonacciIterative(n)
	}
}

// Recursive Exponential Benchmarks

func BenchmarkRecursiveExponential10(b *testing.B) {
	benchmarkRecursiveExponential(10, b)
}

func BenchmarkRecursiveExponential100(b *testing.B) {
	benchmarkRecursiveExponential(100, b)
}

func BenchmarkRecursiveExponential1000(b *testing.B) {
	benchmarkRecursiveExponential(1000, b)
}

func BenchmarkRecursiveExponential10000(b *testing.B) {
	benchmarkRecursiveExponential(10000, b)
}


// Iterative Exponential Benchmarks

func BenchmarkIterativeExponential10(b *testing.B) {
	benchmarkIterativeExponential(10, b)
}

func BenchmarkIterativeExponential100(b *testing.B) {
	benchmarkIterativeExponential(100, b)
}

func BenchmarkIterativeExponential1000(b *testing.B) {
	benchmarkIterativeExponential(1000, b)
}

func BenchmarkIterativeExponential10000(b *testing.B) {
	benchmarkIterativeExponential(10000, b)
}


// Recursive Logarithm Exponential Benchmarks

func BenchmarkRecursiveLogarithmExponential10(b *testing.B) {
	benchmarkRecursiveLogarithmExponential(10, b)
}

func BenchmarkRecursiveLogarithmExponential100(b *testing.B) {
	benchmarkRecursiveLogarithmExponential(100, b)
}

func BenchmarkRecursiveLogarithmExponential1000(b *testing.B) {
	benchmarkRecursiveLogarithmExponential(1000, b)
}

func BenchmarkRecursiveLogarithmExponential10000(b *testing.B) {
	benchmarkRecursiveLogarithmExponential(10000, b)
}


// Iterative Logarithm Exponential Benchmarks

func BenchmarkIterativeLogarithmExponential10(b *testing.B) {
	benchmarkIterativeLogarithmExponential(10, b)
}

func BenchmarkIterativeLogarithmExponential100(b *testing.B) {
	benchmarkIterativeLogarithmExponential(100, b)
}

func BenchmarkIterativeLogarithmExponential1000(b *testing.B) {
	benchmarkIterativeLogarithmExponential(1000, b)
}

func BenchmarkIterativeLogarithmExponential10000(b *testing.B) {
	benchmarkIterativeLogarithmExponential(10000, b)
}

// Recursive Fibonacci Benchmarks

func BenchmarkRecursiveFibonacci10(b *testing.B) {
	benchmarkRecursiveFibonacci(10, b)
}

func BenchmarkRecursiveFibonacci20(b *testing.B) {
	benchmarkRecursiveFibonacci(20, b)
}

func BenchmarkRecursiveFibonacci30(b *testing.B) {
	benchmarkRecursiveFibonacci(30, b)
}

func BenchmarkRecursiveFibonacci50(b *testing.B) {
	benchmarkRecursiveFibonacci(50, b)
}

// Iterative Fibonacci Benchmarks

func BenchmarkIterativeFibonacci10(b *testing.B) {
	benchmarkIterativeFibonacci(10, b)
}

func BenchmarkIterativeFibonacci20(b *testing.B) {
	benchmarkIterativeFibonacci(20, b)
}

func BenchmarkIterativeFibonacci30(b *testing.B) {
	benchmarkIterativeFibonacci(30, b)
}

func BenchmarkIterativeFibonacci50(b *testing.B) {
	benchmarkIterativeFibonacci(50, b)
}